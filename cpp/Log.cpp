#include "Log.h"

std::ofstream g_sLogFile("hwo.log");
static TeeDevice gs_tLogTee(std::cout, g_sLogFile); 
TeeStream g_sLog(gs_tLogTee);


static void PrintLineBreak(int iIndent)
{
	g_sLog << std::endl;
	for(int i = 0; i < iIndent; i++)
		g_sLog << "  ";
}

void PrintJSON(const jsoncons::json& data)
{

	std::string s = data.to_string();

	int iIndent = 0;
	for(auto p = s.begin(); p != s.end(); p++)
	{
		if(*p == '{' || *p == '[')
		{
			PrintLineBreak(iIndent);
		}
		else if(*p == '}' || *p == ']')
		{
			--iIndent;
			PrintLineBreak(iIndent);
		}

		g_sLog << *p;

		if(*p == '{' || *p == '[')
		{
			++iIndent;
			if(*(p + 1) != '{' && *(p + 1) != '[')
			{
				PrintLineBreak(iIndent);
			}
		}
		else if(*p == ',' && *(p + 1) != '{' && *(p + 1) != '[')
		{
			PrintLineBreak(iIndent);
		}
	}

	g_sLog << std::endl;
}