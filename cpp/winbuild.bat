g++ ^
	main.cpp ^
	protocol.cpp ^
	connection.cpp ^
	game_logic.cpp ^
	AI.cpp ^
	Log.cpp ^
	-D _WIN32_WINNT=0x0501 ^
	-D WINVER=0x0501 ^
	-std=c++11 ^
	-I C:/boost_1_55_0 ^
	-I jsoncons/src/ ^
	-l ws2_32 ^
	-L C:/boost_1_55_0/lib/mingw_x86 ^
	-l boost_system-mgw48-mt-1_55 ^
	-o plusbot
