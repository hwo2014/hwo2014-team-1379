#ifndef _DATA_H_
#define _DATA_H_

#include <string>
#include <map>
#include <jsoncons/json.hpp>
#include <cassert>

#define MAX_LANES 4

static const double PI = 3.141592653589793238463;
static const float kfPiOver180 = (float)(PI / 180.0);

class CTrackData
{
public:
	std::string m_sID;
	std::string m_sName;

	struct TPieceData
	{
		float m_fLength{ 0.f };
		float m_fRadius { 0.f };
		float m_fAngle { 0.f };
		bool m_bSwitch { false };

		float GetLength(float fLaneOffset = 0.f) const
		{
			if(m_fLength > 0.f)
			{
				return m_fLength;
			}
			else
			{
				float fR = m_fRadius;
				if(m_fAngle < 0.f)
					fR += fLaneOffset;
				else
					fR -= fLaneOffset;
				return abs(fR * m_fAngle * kfPiOver180);
			}
		}
	};
	TPieceData* m_pPieces{ nullptr };
	int m_iNumPieces;

	float m_fLaneOffsets[MAX_LANES];
	int m_iNumLanes;

	float m_fStartX;
	float m_fStartY;
	float m_fStartAngle;

	void Init(const jsoncons::json& jTrackMsg)
	{
		m_sID = jTrackMsg["id"].as_string();
		m_sName = jTrackMsg["name"].as_string();

		const auto& jPieces = jTrackMsg["pieces"];
		if(m_pPieces != nullptr)
		{
			std::cerr << "Realloc CTrackData::m_pPieces" << std::endl;
			delete[] m_pPieces;
		}
		m_iNumPieces = jPieces.size();
		m_pPieces = new TPieceData[m_iNumPieces];
		for(int i = 0; i < m_iNumPieces; i++)
		{
			const auto& jPiece = jPieces[i];
			if(jPiece.has_member("length"))
				m_pPieces[i].m_fLength = jPiece["length"].as_double();
			
			if(jPiece.has_member("radius"))
				m_pPieces[i].m_fRadius = jPiece["radius"].as_double();
			
			if(jPiece.has_member("angle"))
				m_pPieces[i].m_fAngle = jPiece["angle"].as_double();
			
			if(jPiece.has_member("switch"))
				m_pPieces[i].m_bSwitch = jPiece["switch"].as_bool();
		}

		const auto& jLanes = jTrackMsg["lanes"];
		m_iNumLanes = jLanes.size();
		for(auto p = jLanes.begin_elements(); p != jLanes.end_elements(); p++)
		{
			m_fLaneOffsets[(*p)["index"].as_int()] = (*p)["distanceFromCenter"].as_double();
		}

		const auto& jStart = jTrackMsg["startingPoint"];
		m_fStartX = jStart["position"]["x"].as_double();
		m_fStartY = jStart["position"]["y"].as_double();
		m_fStartAngle = jStart["angle"].as_double();
	}

	float GetLapPos(int iPiece, float fInPieceDist) const
	{
		float fRet = 0.f;
		for(int i = 0; i < iPiece; i++)
		{
			fRet += m_pPieces[i].GetLength(0.f);
		}
		fRet += fInPieceDist;
		return fRet;
	}
};

class CCarData
{
public:
	// const info
	const CTrackData* m_pTrackData{ nullptr };
	std::string m_sName;
	std::string m_sColor;
	float m_fLength;
	float m_fWidth;
	float m_fGuideFlagPos;

	// variable info
	float m_fAngle{ 0.f };
	int m_iCurrPiece{ 0 };
	float m_fInPieceDistance{ 0.f };
	int m_iCurrLane1{ 0 };
	int m_iCurrLane2{ 0 };
	int m_iLap{ -1 };
	bool m_bCrashed{ false };

	// calculated variable info
	float m_fLastFrameSpeed{ 0.f };
	float m_fLastFrameAccel{ 0.f };

	void SetTrackData(const CTrackData* pData) { m_pTrackData = pData; }

	void Init(const jsoncons::json& jCarMsg)
	{
		const auto& jID = jCarMsg["id"];
		m_sName = jID["name"].as_string();
		m_sColor = jID["color"].as_string();

		const auto& jDimensions = jCarMsg["dimensions"];
		m_fLength = jDimensions["length"].as_double();
		m_fWidth = jDimensions["width"].as_double();
		m_fGuideFlagPos = jDimensions["guideFlagPosition"].as_double();		
	}

	void Update(const jsoncons::json& jPositionMsg)
	{
		CCarData tLast = *this;

		// update basic info
		m_fAngle = jPositionMsg["angle"].as_double();
		
		const auto& jPieceData = jPositionMsg["piecePosition"];
		m_iCurrPiece = jPieceData["pieceIndex"].as_int();
		m_fInPieceDistance = jPieceData["inPieceDistance"].as_double();
		m_iCurrLane1 = jPieceData["lane"]["startLaneIndex"].as_int();
		m_iCurrLane2 = jPieceData["lane"]["endLaneIndex"].as_int();
		m_iLap = jPieceData["lap"].as_int();

		assert(m_pTrackData);

		// calculate extra info
		m_fLastFrameSpeed = 0.f;
		float fPrevLaneOffset = m_pTrackData->m_fLaneOffsets[m_iCurrLane1];
		if(tLast.m_iLap < m_iLap && tLast.m_iLap > -1)
		{
			for(int iPiece = tLast.m_iCurrPiece; iPiece < m_pTrackData->m_iNumPieces; iPiece++)
			{
				m_fLastFrameSpeed += m_pTrackData->m_pPieces[iPiece].GetLength(fPrevLaneOffset);
			}
			for(int iPiece = 0; iPiece < m_iCurrPiece; iPiece++)
			{
				m_fLastFrameSpeed += m_pTrackData->m_pPieces[iPiece].GetLength(fPrevLaneOffset);
			}
		}
		for(int iPiece = tLast.m_iCurrPiece; iPiece < m_iCurrPiece; iPiece++)
		{
			m_fLastFrameSpeed += m_pTrackData->m_pPieces[iPiece].GetLength(fPrevLaneOffset);
		}
		m_fLastFrameSpeed -= tLast.m_fInPieceDistance;
		m_fLastFrameSpeed += m_fInPieceDistance;

		m_fLastFrameAccel = m_fLastFrameSpeed - tLast.m_fLastFrameSpeed;
	}
};

class CRaceData
{
public:
	int m_iLaps{ 0 };
	int m_iMaxLapTimeMS { 0 };
	bool m_bQuickRace{ false };

	void Init(const jsoncons::json& jRaceSessionMsg)
	{
		if(jRaceSessionMsg.has_member("laps"))
			m_iLaps = jRaceSessionMsg["laps"].as_int();

		if(jRaceSessionMsg.has_member("maxLapTimeMs"))
			m_iMaxLapTimeMS = jRaceSessionMsg["maxLapTimeMs"].as_int();

		if(jRaceSessionMsg.has_member("quickRace"))
			m_bQuickRace = jRaceSessionMsg["quickRace"].as_bool();
	}
};

typedef std::map<std::string, CCarData> TCarMap;

#endif // _DATA_H_