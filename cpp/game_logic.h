#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include "Data.h"
#include "AI.h"

class game_logic
{
public:
	typedef std::vector<jsoncons::json> msg_vector;

	game_logic();
	msg_vector react(const jsoncons::json& msg);

private:
	typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
	std::map<std::string, action_fun> action_map;

	msg_vector on_join(const jsoncons::json& data);
	msg_vector on_your_car(const jsoncons::json& data);
	msg_vector on_game_init(const jsoncons::json& data);
	msg_vector on_game_start(const jsoncons::json& data);
	msg_vector on_car_positions(const jsoncons::json& data);
	msg_vector on_crash(const jsoncons::json& data);
	msg_vector on_spawn(const jsoncons::json& data);
	msg_vector on_lap_finished(const jsoncons::json& data);
	msg_vector on_dnf(const jsoncons::json& data);
	msg_vector on_finish(const jsoncons::json& data);
	msg_vector on_game_end(const jsoncons::json& data);
	msg_vector on_tournament_end(const jsoncons::json& data);
	msg_vector on_error(const jsoncons::json& data);
	
	CRaceData m_tRaceData;
	CTrackData m_tTrackData;
	TCarMap m_tCarData;
	CAI m_tAI;
};

#endif
