#ifndef _LOG_H_
#define _LOG_H_

#include <fstream>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>
#include "jsoncons/json.hpp"

// make a log file that also prints to stdout
typedef boost::iostreams::tee_device<std::ostream, std::ofstream> TeeDevice;
typedef boost::iostreams::stream<TeeDevice> TeeStream;
extern std::ofstream g_sLogFile;
extern TeeStream g_sLog;

void PrintJSON(const jsoncons::json& data);

#endif // _LOG_H_