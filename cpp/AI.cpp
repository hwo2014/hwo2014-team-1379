#include "AI.h"
#include <boost/algorithm/clamp.hpp>
#include "Log.h"

#define COLLECT_DATA
#ifdef COLLECT_DATA
std::ofstream g_sData("data.csv");
#endif

//--------------------------------------------------------------------

CAI::CAI(const CTrackData& tTrackData, const TCarMap& tCarMap)
: m_tTrackData(tTrackData)
, m_tCarMap(tCarMap)
{
#ifdef COLLECT_DATA
	g_sData << "fCurrentSpeed, fTargetSpeed, fTargetAccel, fForceRequired (est), fThrottle" << std::endl;
#endif
}

//--------------------------------------------------------------------

void CAI::SetColour(std::string sColour)
{
	m_sMyColour = sColour;
}

//--------------------------------------------------------------------

void CAI::OnTrackDataInitialised()
{
	// map out target speeds for the start of each piece,
	// then we can lerp between them at race-time
	for(int i = 0; i < m_tTrackData.m_iNumPieces; i++)
	{
		// TODO: determine magic numbers from track grip info
		float fSpeed = 10.f; // magic

		const float kfMaxLookahead = 150.f;
		float fCurrLookahead = 0.f;
		int iPiece = i;
		while(fCurrLookahead < kfMaxLookahead)
		{
			const CTrackData::TPieceData& tPiece = m_tTrackData.m_pPieces[iPiece];
			float fLength = tPiece.GetLength();
			if(fLength > kfMaxLookahead - fCurrLookahead)
				fLength = kfMaxLookahead - fCurrLookahead;
			
			if(tPiece.m_fAngle)
			{
				float fScale = 2.f*kfMaxLookahead / (fCurrLookahead + 2.f*kfMaxLookahead); // scales linearly in range [0.66, 1] based on distance
				fScale *= 3.f; // magic

				// centripetal acceleration:
				// a = v^2 / r
				// => F = mv^2 / r
				// find a cap for a, using car's centre of mass vs guide flag pos
				// (max drift = 90deg?)
				// then accumulate a over the length of each piece
				//
				// perhaps there is also friction from the wheels, perpendicular to this
				// there will definitely also be torque from the guide flag being pulled forwards along the track -
				// but how do we calculate this? need inertia matrix which needs mass
				//
				// a applied to COM => guide_flag_offset * a rotational accel
				// bear in mind, as car theta increases, guide_flag_offset decreases:
				// offset = gfo * cos(theta_current)
				// this can be integrated...


				fSpeed -= fScale * fLength / tPiece.m_fRadius;
			}

			++iPiece;
			if(iPiece == m_tTrackData.m_iNumPieces)
				iPiece = 0;
			fCurrLookahead += fLength + 0.001f;
		}

		fSpeed = boost::algorithm::clamp(fSpeed, 0.f, 10.f);
		m_fTargetSpeeds.push_back(fSpeed);

		const CTrackData::TPieceData& tThisPiece = m_tTrackData.m_pPieces[i];
		g_sLog << "Piece " << i << ": radius " << tThisPiece.m_fRadius  << ", length: " << tThisPiece.GetLength() << " --> speed " << fSpeed << std::endl;
	}
}

//--------------------------------------------------------------------

void CAI::OnCrash()
{
	const CCarData& tMyCar = m_tCarMap.at(m_sMyColour);

	// log why we crashed
	if(m_fLastFrameTargetSpeed > tMyCar.m_fLastFrameSpeed)
		g_sLog << "CRASHED: Target speed (" << m_fLastFrameTargetSpeed << ") too high for piece " << tMyCar.m_iCurrPiece << std::endl;
	else
		g_sLog << "CRASHED: Failed to keep to target speed (" << m_fLastFrameTargetSpeed << "), actual speed was " << tMyCar.m_fLastFrameSpeed << std::endl;
}

//--------------------------------------------------------------------

float CAI::GetThrottle()
{
	const CCarData& tMyCar = m_tCarMap.at(m_sMyColour);
	
	if(tMyCar.m_bCrashed)
		return 0.f;

	// lerp a target speed from precomputed points
	float fCurrLaneOffset = m_tTrackData.m_fLaneOffsets[tMyCar.m_iCurrLane1];
	int iCurrPiece = tMyCar.m_iCurrPiece;
	int iNextPiece = iCurrPiece + 1;
	if(iNextPiece == m_tTrackData.m_iNumPieces)
		iNextPiece = 0;
	float fInterp = tMyCar.m_fInPieceDistance / m_tTrackData.m_pPieces[tMyCar.m_iCurrPiece].GetLength(fCurrLaneOffset);
	float fTargetSpeed = m_fTargetSpeeds[iCurrPiece] + (m_fTargetSpeeds[iNextPiece] - m_fTargetSpeeds[iCurrPiece]) * fInterp;
	m_fLastFrameTargetSpeed = fTargetSpeed;

	// drop off if we're drifting too much
//	const float kfCarDriftTolerance = 45.f;
//	fTargetSpeed *= 1.f - (abs(tMyCar.m_fAngle) / kfCarDriftTolerance);
//	g_sLog << "anti-drift: " << 1.f - (abs(tMyCar.m_fAngle) / kfCarDriftTolerance) << std::endl;

	// how do we find our desired acceleration?
	const float kfAccelSmooth = 0.25f; // magic - increase this to be more aggressive on speed
	float fTargetAccel = (fTargetSpeed - tMyCar.m_fLastFrameSpeed) * kfAccelSmooth;

	float fAccelDeficit = m_fLastFrameTargetAccel - tMyCar.m_fLastFrameAccel;
	m_fLastFrameTargetAccel = fTargetAccel;
	
	// friction force = -mu * v
	// drag force = -d * v^2
	// so, resistance (approx)= -mu * v - d * pow(v, 2) for some mu, d
	// assume mu is negligible, then
	// m*a = F = f_engine - d*v^2, hence
	// f_engine = m*acc_desired + d*v^2
	// constants we need to find: mass (m) and drag coefficient (d)
	static float s_fDragConst = 0.01f;	// }
	static float s_fMass = 1.f;			// } tweak these two to minimise abs(fAccelDeficit)
	if(tMyCar.m_fLastFrameAccel != 0.f && tMyCar.m_fLastFrameSpeed != 0.f)
	{
		float fDragEst = (m_fLastFrameThrottle - s_fMass * tMyCar.m_fLastFrameAccel) / pow(tMyCar.m_fLastFrameSpeed, 2);
		const float kfWeight = 0.5f;
		s_fDragConst = s_fDragConst + (fDragEst - s_fDragConst) * kfWeight;
		g_sLog << "New drag constant: " << s_fDragConst << std::endl;
	}
	float fForceRequired = s_fMass * fTargetAccel + s_fDragConst * pow(tMyCar.m_fLastFrameSpeed, 2);

	// TODO: test if force is proportional to throttle
	//static float s_fEngineMaxForce = 1.f; // find this in test runs by setting throttle to 1.f
	// does engine force scale linearly with throttle? if so,
	float fThrottle = fForceRequired;// / s_fEngineMaxForce;
	g_sLog
		<< "fCurrentSpeed = " << tMyCar.m_fLastFrameSpeed
		<< "\tfTargetSpeed = " << fTargetSpeed
		<< "\tfTargetAccel = " << fTargetAccel
		<< "\tfForceRequired = " << fForceRequired
		<< "\tfThrottle = " << fThrottle
		<< std::endl;
	
#ifdef COLLECT_DATA
	g_sData << tMyCar.m_fLastFrameSpeed << "," << fTargetSpeed << ","  << fTargetAccel << ","  << fForceRequired << ","  << fThrottle << std::endl;
#endif

	fThrottle = boost::algorithm::clamp(fThrottle, 0.f, 1.f);
	m_fLastFrameThrottle = fThrottle;
	return fThrottle;
}

//--------------------------------------------------------------------

CAI::ELaneSwitch CAI::GetLaneSwitch()
{
	const CCarData& tMyCar = m_tCarMap.at(m_sMyColour);
	int iNextPiece = tMyCar.m_iCurrPiece + 1;
	if(iNextPiece == m_tTrackData.m_iNumPieces)
		iNextPiece = 0;
	
	// only check once per piece
	if(m_iLastSwitchCheckPiece == iNextPiece)
		return ELaneSwitch::None;

	m_iLastSwitchCheckPiece = iNextPiece;

	if(m_tTrackData.m_pPieces[iNextPiece].m_bSwitch == false)
		return ELaneSwitch::None;

	// find the next switch after this piece
	// add up the sum distance for each lane
	float fLaneDistances[MAX_LANES] = { 0 };
	for(int iPiece = iNextPiece; ; )
	{
		++iPiece;
		if(iPiece == m_tTrackData.m_iNumPieces)
			iPiece = 0;

		if(m_tTrackData.m_pPieces[iPiece].m_bSwitch)
			break;

		for(int iLane = 0; iLane < m_tTrackData.m_iNumLanes; iLane++)
		{
			fLaneDistances[iLane] += m_tTrackData.m_pPieces[iPiece].GetLength(m_tTrackData.m_fLaneOffsets[iLane]);
		}
	}

	// find the minimum
	int iBestLane = tMyCar.m_iCurrLane1; // default to our current lane, in case all lanes are equal
	for(int iLane = 0; iLane < m_tTrackData.m_iNumLanes; iLane++)
		if(fLaneDistances[iLane] < fLaneDistances[iBestLane])
			iBestLane = iLane;

	if(m_tTrackData.m_fLaneOffsets[iBestLane] < m_tTrackData.m_fLaneOffsets[tMyCar.m_iCurrLane1])
	{
		g_sLog << "Switch LEFT towards lane " << iBestLane << std::endl;
		return ELaneSwitch::Left;
	}
	if(m_tTrackData.m_fLaneOffsets[iBestLane] > m_tTrackData.m_fLaneOffsets[tMyCar.m_iCurrLane1])
	{
		g_sLog << "Switch RIGHT towards lane " << iBestLane << std::endl;
		return ELaneSwitch::Right;
	}

	return ELaneSwitch::None;
}