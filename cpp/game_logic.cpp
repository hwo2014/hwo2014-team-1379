#include "game_logic.h"
#include "protocol.h"
#include "Log.h"

using namespace hwo_protocol;

game_logic::game_logic()
: m_tAI(m_tTrackData, m_tCarData)
{
	// workaround for VC++ compile error
	action_map["join"] = std::mem_fn(&game_logic::on_join);
	action_map["yourCar"] = std::mem_fn(&game_logic::on_your_car);
	action_map["gameInit"] = std::mem_fn(&game_logic::on_game_init);
	action_map["gameStart"] = std::mem_fn(&game_logic::on_game_start);
	action_map["carPositions"] = std::mem_fn(&game_logic::on_car_positions);
	action_map["crash"] = std::mem_fn(&game_logic::on_crash);
	action_map["spawn"] = std::mem_fn(&game_logic::on_spawn);
	action_map["lapFinished"] = std::mem_fn(&game_logic::on_lap_finished);
	action_map["dnf"] = std::mem_fn(&game_logic::on_dnf);
	action_map["gameEnd"] = std::mem_fn(&game_logic::on_game_end);
	action_map["tournamentEnd"] = std::mem_fn(&game_logic::on_tournament_end);
	action_map["error"] = std::mem_fn(&game_logic::on_error);
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	auto action_it = action_map.find(msg_type);
	if (action_it != action_map.end())
	{
		return (action_it->second)(this, data);
	}
	else
	{
		g_sLog << "Unknown message type: " << msg_type << std::endl;
		return { make_ping() };
	}
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
	g_sLog << "Joined" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
	g_sLog << "My car has name: " << data["name"] << ", colour: " << data["color"] << std::endl;
	m_tAI.SetColour(data["color"].as_string());
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
//	PrintJSON(data);

	// parse track layout
	if(data.has_member("race"))
	{
		const auto& jRace = data["race"];
		if(jRace.has_member("track"))
		{
			m_tTrackData.Init(jRace["track"]);
		}
		if(jRace.has_member("cars"))
		{
			const auto& jCars = jRace["cars"];
			for(auto p = jCars.begin_elements(); p != jCars.end_elements(); p++)
			{
				CCarData tNewCar;
				tNewCar.SetTrackData(&m_tTrackData);
				tNewCar.Init(*p);
				m_tCarData[(*p)["id"]["color"].as_string()] = tNewCar;
			}
		}
		if(jRace.has_member("raceSession"))
		{
			m_tRaceData.Init(jRace["raceSession"]);
		}
	}

	m_tAI.OnTrackDataInitialised();

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	g_sLog << "Race started" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	for(auto p = data.begin_elements(); p != data.end_elements(); p++)
	{
		m_tCarData[(*p)["id"]["color"].as_string()].Update(*p);
	}

	CAI::ELaneSwitch eLane = m_tAI.GetLaneSwitch();
	switch(eLane)
	{
	case CAI::ELaneSwitch::Left:
		return { make_lane_switch(false) };
	case CAI::ELaneSwitch::Right:
		return { make_lane_switch(true) };
	case CAI::ELaneSwitch::None:
	default:
		break;
	}

	float fThrottle = m_tAI.GetThrottle();
	return { make_throttle(fThrottle) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	g_sLog << data["name"] << " (" << data["color"] << ") crashed" << std::endl;
	m_tCarData[data["color"].as_string()].m_bCrashed = true;
	m_tAI.OnCrash();
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
	g_sLog << data["name"] << " (" << data["color"] << ") respawned" << std::endl;
	m_tCarData[data["color"].as_string()].m_bCrashed = false;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
	PrintJSON(data);
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_dnf(const jsoncons::json& data)
{
	PrintJSON(data);
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	PrintJSON(data);
	std::cout << "Race ended" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
	PrintJSON(data);
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
	g_sLog << "Server error: " << data.to_string() << std::endl;
	return { make_ping() };
}
