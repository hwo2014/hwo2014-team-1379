#ifndef _AI_H_
#define _AI_H_

#include "Data.h"

class CAI
{
public:
	enum class ELaneSwitch
	{
		None,
		Left,
		Right
	};

	CAI(const CTrackData& tTrackData, const TCarMap& tCarMap);
	
	void OnTrackDataInitialised();
	void OnCrash();

	float GetThrottle();
	ELaneSwitch GetLaneSwitch();

	void SetColour(std::string sColour);
	std::string GetColour() { return m_sMyColour; }

private:
	const CTrackData& m_tTrackData;
	const TCarMap& m_tCarMap;
	std::string m_sMyColour;

	std::vector<float> m_fTargetSpeeds;
	float m_fLastFrameTargetSpeed{ 0.f };
	float m_fLastFrameTargetAccel{ 0.f };
	float m_fLastFrameThrottle{ 0.f };
	int m_iLastSwitchCheckPiece{ -1 };
};

#endif // AI_H_